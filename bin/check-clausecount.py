#!/usr/bin/env python3

import sys
import re
import json
import os
import argparse

from subprocess import Popen, PIPE, TimeoutExpired


def normalize(string):
    string = re.sub(r'\s+$', '', string)
    return string


def test(total, type, max, exact):
    passes = True
    if (exact is not None):
        req = " (required: %d)" % exact
        if (total != exact):
            passes = False
    elif (max is not None):
        req = " (max permitted: %d)" % max
        if (total > max):
            passes = False
    else:
        req = ""

    print("%s: %d%s" % (type, total, req))
    return passes


def main(args):
    statsBinary = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "prolog-stats.jar")
    studentScript = args.source_file
    if not os.path.isfile(studentScript) or not os.access(studentScript, os.R_OK):
        print("Failed to read %s" % os.path.basename(studentScript))
        return False
    timeout = 30
    process = Popen(["java", "-jar", statsBinary,
                     studentScript], stdout=PIPE, stderr=PIPE)
    try:
        (output, err) = map(lambda x: normalize(
            x.decode("utf-8")), process.communicate(timeout=timeout))
        exit_code = process.wait(timeout=timeout)
    except TimeoutExpired:
        print("Script timed out!")
        return False

    if (err != ""):
        print(err)

    if (exit_code != 0):
        print("Parser failed to execute")
        return False

    stats = json.loads(output)

    rules = int(stats["rules"])
    facts = int(stats["facts"])
    queries = int(stats["queries"])

    passed = True
    passed = test(facts, "Facts", args.max_facts, args.facts) and passed
    passed = test(rules, "Rules", args.max_rules, args.rules) and passed
    passed = test(queries, "Queries", args.max_queries,
                  args.queries) and passed
    passed = test(facts+rules+queries, "Total",
                  args.max_clauses, args.clauses) and passed
    return passed


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--source_file', required=True)
    parser.add_argument('--max_clauses',type=int)
    parser.add_argument('--max_rules',type=int)
    parser.add_argument('--max_facts',type=int)
    parser.add_argument('--max_queries',type=int)
    parser.add_argument('--queries',type=int)
    parser.add_argument('--rules',type=int)
    parser.add_argument('--facts',type=int)
    parser.add_argument('--clauses',type=int)
    args = parser.parse_args()

    if (main(args)):
        print("Pass")
        sys.exit(0)
    else:
        print("Fail")
        sys.exit(-1)
