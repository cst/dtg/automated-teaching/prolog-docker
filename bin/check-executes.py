#!/usr/bin/env python3

import sys
import re
import os
from subprocess import Popen, PIPE, TimeoutExpired
import argparse


def normalize(string):
    string = re.sub(r'\s+$', '', string)
    return string


def main(studentScript, timeout):
    if not os.path.isfile(studentScript) or not os.access(studentScript, os.R_OK):
        print("Failed to read %s" % os.path.basename(studentScript))
        return False
        
    print("Executing %s" % os.path.basename(studentScript))
    process = Popen(["swipl", "-g", "consult(['%s'])" % studentScript, "-g", "halt"], stdout=PIPE,stderr=PIPE)
    try:
        (output,err) = map(lambda x:normalize(x.decode("utf-8")),process.communicate(timeout=timeout))
        exit_code = process.wait(timeout=timeout)
    except TimeoutExpired:
        print("Script timed out!")
        return False

    if (err != ""):
        print(err) 
    
    if (exit_code != 0):
        print("Script failed to execute")
        return False

    print("Expected non-empty output")
    print("Actual output:")
    print("----")
    print(output)
    print("----")
    if output != "":
        return True
    else:
        return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--source_file', required=True)
    parser.add_argument('--timeout_seconds', type=int, required=True)
    args = parser.parse_args()

    if (main(args.source_file, args.timeout_seconds)):
        print("Pass")
        sys.exit(0)
    else:
        print("Fail")
        sys.exit(-1)
