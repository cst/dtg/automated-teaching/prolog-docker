#!/usr/bin/env python3

import sys
import re
import os
from subprocess import Popen, PIPE, STDOUT, TimeoutExpired
import argparse


def normalize(string):
    string = re.sub(r'\s+$', '', string)
    return string


def main(scripts, expectedOutput, timeout, regexp_match, stack_limit, ignore_exitcode, negate_match):
    for script in scripts:
        if not os.path.isfile(script) or not os.access(script, os.R_OK):
            print("Failed to read %s" % script)
            return False

    args = ["swipl", "--stack_limit=%s" % stack_limit]
    for script in scripts:
        args.append("-g")
        args.append("consult(['%s'])" % script)
    args.append("-g")
    args.append("halt")
    process = Popen(args, stdout=PIPE, stderr=PIPE)

    try:
        (output, err) = map(lambda x: normalize(
            x.decode("utf-8")), process.communicate(timeout=timeout))
        exit_code = process.wait(timeout=timeout)
    except TimeoutExpired:
        process.kill()
        (output, err) = map(lambda x: normalize(
            x.decode("utf-8")), process.communicate())
        print(normalize(output + "\n" + err))
        print("Script timed out!")
        return False

    output = normalize(output + "\n" + err)

    if (not ignore_exitcode) and exit_code != 0:
        print("Script failed to execute")
        return False

    if expectedOutput is None:
        print("Script executed successfully")
        return True

    if regexp_match:
        if negate_match:
            print("Output must not match (regular expression)")
        else:
            print("Expected output (regular expression match):")
    else:
        if negate_match:
            print("Output must not equal")
        else:
            print("Expected output:")
    print("----")
    with open(expectedOutput, 'r') as file:
        expected = normalize(file.read())
    print(expected)
    print("----")

    print("Actual output:")
    print("----")
    print(output)
    print("----")

    if regexp_match:
        success = re.match(expected, output) is not None or re.match(
            expected, output+"\n") is not None
    else:
        success = (output == expected) or (output+"\n" == expected)

    if negate_match:
        return not success
    else:
        return success


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--source_file', required=True, nargs='+')
    parser.add_argument('--expected_output', required=False)
    parser.add_argument('--regexp_match', type=bool, default=False)
    parser.add_argument('--timeout_seconds', type=int, required=True)
    parser.add_argument('--stack_limit', required=False, default="50M")
    parser.add_argument('--ignore_exitcode', type=bool,
                        required=False, default=False)
    parser.add_argument('--negate_match', type=bool,
                        required=False, default=False)

    args = parser.parse_args()

    if (main(args.source_file, args.expected_output, args.timeout_seconds, args.regexp_match, args.stack_limit, args.ignore_exitcode, args.negate_match)):
        print("Pass")
        sys.exit(0)
    else:
        print("Fail")
        sys.exit(-1)
