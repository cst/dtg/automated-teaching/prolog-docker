FROM openjdk:14-buster
ARG container_userid

RUN apt-get update && \
    apt-get install -y --no-install-recommends \ 
       swi-prolog-nox maven python3 \
       && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /pottery-binaries
ADD https://gitlab.developers.cam.ac.uk/api/v4/projects/3274/repository/branches /tmp/java-accessor-version.json
RUN git clone https://gitlab.developers.cam.ac.uk/cst/dtg/automated-teaching/prolog-stats.git /tmp/prolog-stats
WORKDIR /tmp/prolog-stats
RUN mvn -q -B clean package
RUN cp target/prolog-stats-*-jar-with-dependencies.jar /pottery-binaries/prolog-stats.jar
RUN chmod 444 /pottery-binaries/prolog-stats.jar
RUN rm -rf /tmp/prolog-stats

RUN groupadd user
RUN useradd --shell /bin/bash -u ${container_userid} -o -c "" -m user -g user

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
COPY bin/* /pottery-binaries/
RUN chmod 755 /usr/local/bin/entrypoint.sh /pottery-binaries/*

WORKDIR /home/user
USER user
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
